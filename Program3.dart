void main() {
  App app = new App();
  String name = "Sixty60";
  app.appName = name;
  app.sector = "Best Enterprise Solution";
  app.year = 2020;
  app.capitalName(name);
  print(app.toString());
}

class App {
  String? appName;
  String? sector;
  int? year;

  String capitalName(String data) {
    data = data.toUpperCase();
    return data;
  }

  String toString() {
    var output = "Name: $appName\nSector: $sector\nYear: $year";
    return output;
  }
}
